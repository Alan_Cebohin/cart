// ========================
// 		VARIABLES
// ========================

const cart = $("#cart");
const courses = $("#list-courses");

 

// ========================
// 		LISTENERS
// ========================
loadEventListeners();

function loadEventListeners() {
	// Triggers when press "add to cart"
	courses.click(buyCourse);
}


// ========================
// 		FUNCTIONS
// ========================

// Add course to cart
// ___________________
function buyCourse(e) {
	e.preventDefault();
	$('a').unbind('click')

	$('.add-cart').click(function(e) {
		e.preventDefault();

		if ($(e.target).hasClass('add-cart')) {
			const course = $(e.target);

			console.log(course);
		}
	});
}

// Test change made from terminal 
